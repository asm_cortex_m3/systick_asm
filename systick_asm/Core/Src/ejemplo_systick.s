        .syntax     unified
        .cpu        cortex-m0
        .thumb

        .section    .text               //Declaro que estoy en la FLASH
        .align      2                   //Estoy en Cortex-M siempre Thumb-2
        .global     SysTick_Handler     //Etiqueta para que el resto del programa vea SysTick_Handler
        .extern     flag                //Variable que está afuera de este archivo (flag)


/****************************************/
/*  Systick_Handler.                    */
/*  Incrementa la variable flag y se va */
/****************************************/
.type       SysTick_Handler, %function
SysTick_Handler:
        LDR     R0,=flag                //Cargo la dirección de flag
        LDR     R1,[R0]                 //Me traigo lo apuntado por flag a R1
        ADDS    R1,R1,#1                //Incremento R1
        STR     R1,[R0]                 //Actualizo flag
        BX      LR                      //Retorno ¿Cuánto vale LR? ¿Por qué no usé la pila?
.end
